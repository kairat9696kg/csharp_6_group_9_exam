using System;

namespace Csharp_6_Group_9_Exam_Ubaydullaev_Gairatbek.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}
