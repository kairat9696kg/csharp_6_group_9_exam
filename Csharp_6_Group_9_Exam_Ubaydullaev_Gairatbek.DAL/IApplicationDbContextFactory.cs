﻿namespace Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext Create();
    }
}
