﻿using Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.Entities;
using Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.EntitiesConfiguration.Contracts;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, int>
    {
        private readonly IEntityConfigurationsContainer _entityConfigurationsContainer;

        public DbSet<Advertisement> Advertisements { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<GalleryImage> GalleryImages { get; set; }
        public ApplicationDbContext(
            DbContextOptions options,
            IEntityConfigurationsContainer entityConfigurationsContainer) : base(options)
        {
            _entityConfigurationsContainer = entityConfigurationsContainer;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity(_entityConfigurationsContainer.AdvertisementConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.CategoryConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.GalleryImageConfiguration.ProvideConfigurationAction());

            DisableOneToManyCascadeDelete(builder);
        }

        private void DisableOneToManyCascadeDelete(ModelBuilder builder)
        {
            foreach (var relation in builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relation.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }
    }
}
