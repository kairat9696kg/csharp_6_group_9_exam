﻿using System.Collections.Generic;

namespace Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.Entities
{
    public class Category : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Advertisement> Advertisements { get; set; }
    }
}
