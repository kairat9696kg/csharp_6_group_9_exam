﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.Entities
{
    public class Advertisement : IEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int Contacts { get; set; }
        public decimal Price { get; set; }
        public byte[] Image { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public ICollection<GalleryImage> GalleryImages { get; set; }
    }
}
