﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.Entities
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
