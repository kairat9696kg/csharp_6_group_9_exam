﻿using Microsoft.AspNetCore.Identity;
using System.Collections;
using System.Collections.Generic;

namespace Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.Entities
{
    public class User : IdentityUser<int>, IEntity
    {
        public ICollection<Advertisement> Advertisements { get; set; }
    }
}
