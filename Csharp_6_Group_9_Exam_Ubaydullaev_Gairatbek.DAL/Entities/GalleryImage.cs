﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.Entities
{
    public class GalleryImage : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public byte[] Image { get; set; }
        public int AdvertisementId { get; set; }
        public Advertisement Advertisement { get; set; }
    }
}
