﻿using Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfiguration<T> where T : class, IEntity
    {
        Action<EntityTypeBuilder<T>> ProvideConfigurationAction();
    }
}
