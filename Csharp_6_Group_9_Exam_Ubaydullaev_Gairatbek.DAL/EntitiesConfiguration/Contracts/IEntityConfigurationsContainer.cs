﻿using Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfigurationsContainer
    {
        IEntityConfiguration<Advertisement> AdvertisementConfiguration { get; }
        IEntityConfiguration<Category> CategoryConfiguration { get; }
        IEntityConfiguration<GalleryImage> GalleryImageConfiguration { get; }
    }
}
