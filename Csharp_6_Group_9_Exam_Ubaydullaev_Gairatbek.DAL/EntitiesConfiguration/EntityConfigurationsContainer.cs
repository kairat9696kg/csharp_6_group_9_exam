﻿using Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.Entities;
using Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.EntitiesConfiguration.Contracts;

namespace Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.EntitiesConfiguration
{
    public class EntityConfigurationsContainer : IEntityConfigurationsContainer
    {
        public IEntityConfiguration<Advertisement> AdvertisementConfiguration { get; }
        public IEntityConfiguration<Category> CategoryConfiguration { get; }
        public IEntityConfiguration<GalleryImage> GalleryImageConfiguration { get; }

        public EntityConfigurationsContainer()
        {
            AdvertisementConfiguration = new AdvertisementConfiguration();
            CategoryConfiguration = new CategoryConfiguration();
            GalleryImageConfiguration = new GalleryImageConfiguration();
        }
    }
}
