﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.Repositories.Contracts
{
    public interface IUnitOfWorkFactory
    {
        UnitOfWork Create();
    }
}
