﻿using Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.Entities;

namespace Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.Repositories.Contracts
{
    public interface ICategoryRepository : IRepository<Category>
    {
    }
}
