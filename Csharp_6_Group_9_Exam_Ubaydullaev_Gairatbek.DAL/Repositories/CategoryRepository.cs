﻿using Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.Entities;
using Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.Repositories.Contracts;

namespace Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.Repositories
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Categories;
        }
    }
}
