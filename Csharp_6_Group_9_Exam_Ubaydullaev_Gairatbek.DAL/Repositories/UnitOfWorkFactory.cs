﻿using Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.Repositories.Contracts;

namespace Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.Repositories
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private readonly IApplicationDbContextFactory _applicationDbContextFactory;

        public UnitOfWorkFactory(IApplicationDbContextFactory applicationDbContextFactory)
        {
            _applicationDbContextFactory = applicationDbContextFactory;
        }

        public UnitOfWork Create()
        {
            return new UnitOfWork(_applicationDbContextFactory.Create());
        }
    }
}
