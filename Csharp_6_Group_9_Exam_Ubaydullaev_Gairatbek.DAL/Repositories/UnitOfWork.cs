﻿using Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.Repositories.Contracts;
using System;

namespace Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.Repositories
{
    public class UnitOfWork : IDisposable
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed;

        public IAdvertisementRepository Advertisement { get; set; }
        public ICategoryRepository Category { get; set; }
        public IGalleryImageRepository GalleryImage { get; set; }

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;

            Advertisement = new AdvertisementRepository(context);
            Category = new CategoryRepository(context);
            GalleryImage = new GalleryImageRepository(context);
        }

        #region Disposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _context.Dispose();

            _disposed = true;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
        #endregion
    }
}
