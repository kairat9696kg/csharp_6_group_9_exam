﻿using Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.Entities;
using Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.Repositories.Contracts;

namespace Csharp_6_Group_9_Exam_Ubydullaev_Gairatbek.DAL.Repositories
{
    public class GalleryImageRepository : Repository<GalleryImage>,IGalleryImageRepository
    {
        public GalleryImageRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.GalleryImages;
        }
    }

}
